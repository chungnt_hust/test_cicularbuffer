/* */
#include "cicular.h"
#include <math.h>

//cicularBuf_t cBuffer;


void CBUFFER_Init(cicularBuf_t *cBuf, volatile uint8_t *address, uint16_t size)
{
	cBuf->buf  = address;
	cBuf->size = size;
	cBuf->tail = 0;
	cBuf->head = 0;
	cBuf->count = 0;
	cBuf->flagRevEn = false;
}

bool CBUFFER_Full(cicularBuf_t *cBuf)
{
	if(cBuf->count == cBuf->size) return true;
	else return false;
}

bool CBUFFER_Empty(cicularBuf_t *cBuf)
{
	if(cBuf->count == 0) return true;
	else return false;
}
	
void CBUFFER_Put(cicularBuf_t *cBuf, uint8_t data)
{
	cBuf->buf[cBuf->head] = data;
	cBuf->head = (cBuf->head+1) % cBuf->size;
	cBuf->count++;
}

void CBUFFER_Get(cicularBuf_t *cBuf, uint8_t *data)
{
	*data = cBuf->buf[cBuf->tail];
	cBuf->tail = (cBuf->tail+1) % cBuf->size;
	cBuf->count--;
}

void CBUFFER_Putc(cicularBuf_t *cBuf, uint8_t data)
{
	if(CBUFFER_Full(cBuf) == true) return;
	CBUFFER_Put(cBuf, data);
}

void CBUFFER_Getc(cicularBuf_t *cBuf, uint8_t *data)
{
	if(CBUFFER_Empty(cBuf) == true)	return;
	CBUFFER_Get(cBuf, data);
}

void CBUFFER_Reset(cicularBuf_t *cBuf)
{
	cBuf->count = cBuf->tail = cBuf->head = 0;
}
/* baudrate: 115200 ~ 14byte/ms => 2ms check flag 1 lan de xem nhan duoc du lieu hay chua */
