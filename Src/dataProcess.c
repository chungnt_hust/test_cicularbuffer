
#include "dataProcess.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 150
extern UART_HandleTypeDef huart2;
extern uint8_t g_rxStr[MAX_SIZE];
extern uint8_t numByteRx;

static void DATAPROCESS_tachDau(rxType_t *rx)
{
	uint8_t i;
	bool flag = false;
	rx->CNT = 0;
	for(i = 0; i < numByteRx;i++)
	{
		if(flag == true)
		{
			if(g_rxStr[i] == ',' || g_rxStr[i] == ']')
			{
				rx->CNT++;
				rx->LVT[rx->CNT] = i;				
			}
		}
		
		if(g_rxStr[i] == '[')
		{
			rx->LVT[rx->CNT] = i;
			flag = true;
		}
	}
}

/* destination : dich den */
static void DATAPROCESS_tachChuoi(uint8_t *des, uint8_t *src, uint8_t begin, uint8_t length)
{
	memcpy(des, (const char*)(src+begin), length);
	des[length] = '\0';
}

/* tach tu mang rx buffer cac gia tri luu vao value */
void DATAPROCESS_tachGiatri(rxType_t *rx)
{
	uint8_t cnt;
	uint8_t temp[10];
	DATAPROCESS_tachDau(rx);
	for(cnt = 0; cnt < rx->CNT ; cnt++)
	{
		DATAPROCESS_tachChuoi(temp, g_rxStr, rx->LVT[cnt] + 1, rx->LVT[cnt+1] - rx->LVT[cnt] - 1);
		rx->VALUE[cnt] = atoi((const char*)temp);
	}
}


