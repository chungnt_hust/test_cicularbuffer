/**/
#ifndef _CICULAR_BUFFER_H
#define _CICULAR_BUFFER_H
#include "stm32f3xx_hal.h"
#include <stdbool.h>

typedef struct
{
	volatile uint8_t *buf;  // luu dia chi cua mang cBuff can dung
	uint16_t size;          // kich thuoc mang cbuf
	uint16_t tail;          // phan tu doc
	uint16_t head;          // phan tu viet
	uint16_t count;         // bien dem so phan tu da ghi
	bool flagRevEn;
} cicularBuf_t;

void CBUFFER_Init(cicularBuf_t *cBuf, volatile uint8_t *address, uint16_t size);
bool CBUFFER_Full(cicularBuf_t *cBuf);
bool CBUFFER_Empty(cicularBuf_t *cBuf);
void CBUFFER_Put(cicularBuf_t *cBuf, uint8_t data);
void CBUFFER_Get(cicularBuf_t *cBuf, uint8_t *data);
void CBUFFER_Putc(cicularBuf_t *cBuf, uint8_t data);
void CBUFFER_Getc(cicularBuf_t *cBuf, uint8_t *data);
void CBUFFER_Reset(cicularBuf_t *cBuf);
#endif
